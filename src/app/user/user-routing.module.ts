import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserComponent } from './user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MyProfileComponent } from './my-profile/my-profile.component';

const userRoutes: Routes = [
    {
      path: 'user', component: UserComponent, children: [
        { path: 'dashboard', component: DashboardComponent },
        { path: 'me', component: MyProfileComponent },
      ]
    },
  ];
  

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
