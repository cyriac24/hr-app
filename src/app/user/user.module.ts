import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MyProfileComponent } from './my-profile/my-profile.component';
import { CoreModule } from '../core/core.module';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';

@NgModule({
  declarations: [DashboardComponent, MyProfileComponent, UserComponent],
  imports: [
    CommonModule,
    CoreModule,
    UserRoutingModule
  ]
})
export class UserModule { }
