import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    FormsModule,
    CommonModule,
    NoopAnimationsModule,
    MatSidenavModule,
  ],
  exports: [
    MatSidenavModule,
    FormsModule
  ]
})
export class CoreModule { }
