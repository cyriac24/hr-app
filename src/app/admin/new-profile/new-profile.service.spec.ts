import { TestBed } from '@angular/core/testing';

import { NewProfileService } from './new-profile.service';

describe('NewProfileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewProfileService = TestBed.get(NewProfileService);
    expect(service).toBeTruthy();
  });
});
