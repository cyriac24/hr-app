import { Injectable } from '@angular/core';
import { User } from 'src/app/models';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewProfileService {
  private createdUserSubject = new Subject<User>();
  public userCreated = this.createdUserSubject.asObservable();
  constructor() { }

  createProfile(u: User) {
    u.id = 50;
    this.createdUserSubject.next(u);
  }
}
