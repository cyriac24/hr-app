import { Component, OnInit } from '@angular/core';
import { User, UserDoc } from 'src/app/models';
import { NewProfileService } from './new-profile.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-profile',
  templateUrl: './new-profile.component.html',
  styleUrls: ['./new-profile.component.css']
})
export class NewProfileComponent implements OnInit {
  profile: User = new User();
  files: UserDoc[] = [new UserDoc()];
  constructor(public service: NewProfileService, public route: Router) {
  }

  ngOnInit() {
    this.service.userCreated.subscribe((u: User) => {
      this.route.navigate([`/admin/profile`, u.id])
    })
  }

  submitProfile(u: User) {
    this.service.createProfile(u);
  }

  newDoc() {
    this.files.push(new UserDoc);
  }

  uploadDoc(d: UserDoc) {
    alert('file uploaded');
  }

}
