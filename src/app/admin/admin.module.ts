import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { NewProfileComponent } from './new-profile/new-profile.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { ProfileDetailService } from './profile-detail/profile-detail.service';
import { ProfileListService } from './profile-list/profile-list.service';
import { DashboardService } from './dashboard/dashboard.service';
import { CoreModule } from '../core/core.module';
import { AdminComponent } from './admin.component';

@NgModule({
  declarations: [ProfileListComponent, NewProfileComponent, DashboardComponent, ProfileDetailComponent, AdminComponent],
  imports: [
    CommonModule,
    CoreModule,
    AdminRoutingModule,
    NoopAnimationsModule,
  ],
  providers: [
    ProfileDetailService,
    ProfileListService,
    DashboardService
  ]
})
export class AdminModule { }
