import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileListComponent } from './profile-list/profile-list.component';
import { AdminComponent } from './admin.component';
import { ProfileDetailComponent } from './profile-detail/profile-detail.component';
import { NewProfileComponent } from './new-profile/new-profile.component';

const adminRoutes: Routes = [
  {
    path: 'admin', component: AdminComponent, children: [
      { path: 'profiles', component: ProfileListComponent },
      { path: 'profile/new', component: NewProfileComponent },
      { path: 'profile/:id', component: ProfileDetailComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
