import { Component, OnInit } from '@angular/core';
import { ProfileListService } from './profile-list.service';
import { User } from 'src/app/models';

@Component({
  selector: 'app-profile-list',
  templateUrl: './profile-list.component.html',
  styleUrls: ['./profile-list.component.css']
})
export class ProfileListComponent implements OnInit {
  public profileList: User[] = [];

  constructor(profileListService: ProfileListService) { 
    this.profileList = profileListService.profiles;
  }

  ngOnInit() {
  }

}
