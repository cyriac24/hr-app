import { Injectable } from '@angular/core';
import { User } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class ProfileListService {
  public profiles: User[];
  constructor() {
    this.profiles = [
      {
        firstName: 'Jons',
        lastName: 'Cyriac',
        dob: new Date(),
        createdAt: new Date(),
        files: [],
        socialURLs: []
      },
      {
        firstName: 'Jons',
        lastName: 'Cyriac',
        dob: new Date(),
        createdAt: new Date(),
        files: [],
        socialURLs: []
      },
      {
        firstName: 'Jons',
        lastName: 'Cyriac',
        dob: new Date(),
        createdAt: new Date(),
        files: [],
        socialURLs: []
      },
      {
        firstName: 'Jons',
        lastName: 'Cyriac',
        dob: new Date(),
        createdAt: new Date(),
        files: [],
        socialURLs: []
      },
      {
        firstName: 'Jons',
        lastName: 'Cyriac',
        dob: new Date(),
        createdAt: new Date(),
        files: [],
        socialURLs: []
      },
      {
        firstName: 'Jons',
        lastName: 'Cyriac',
        dob: new Date(),
        createdAt: new Date(),
        files: [],
        socialURLs: []
      },
      {
        firstName: 'Jons',
        lastName: 'Cyriac',
        dob: new Date(),
        createdAt: new Date(),
        files: [],
        socialURLs: []
      },
    ]
   }

}
