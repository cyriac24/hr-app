import { Injectable } from '@angular/core';
import { User } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class ProfileDetailService {

  constructor() { }

  getProfile(id) {
    const p: User = {
      firstName: 'Jons',
      lastName: 'Cyriac',
      dob: new Date(),
      createdAt: new Date(),
      files: [],
      socialURLs: []
    };
    return p;
  }
}
