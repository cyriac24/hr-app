import { TestBed } from '@angular/core/testing';

import { ProfileDetailService } from './profile-detail.service';

describe('ProfileDetailService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ProfileDetailService = TestBed.get(ProfileDetailService);
    expect(service).toBeTruthy();
  });
});
