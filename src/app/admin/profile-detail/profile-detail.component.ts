import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProfileDetailService } from './profile-detail.service';
import { User } from 'src/app/models';

@Component({
  selector: 'app-profile-detail',
  templateUrl: './profile-detail.component.html',
  styleUrls: ['./profile-detail.component.css']
})
export class ProfileDetailComponent implements OnInit {
  profile: User = null;
  constructor(
    private route: ActivatedRoute,
    private service: ProfileDetailService
  ) {
    const id = route.paramMap.subscribe((paramMap) => {
      this.profile = service.getProfile(paramMap.get('id'));
    })
  }

  ngOnInit() {
  }

}
