export class User {
    firstName: String
    lastName: String
    dob: Date
    createdAt: Date
    files: Object[]
    socialURLs: Object[]
    id?: Number
}

export class UserDoc {
    required: Boolean
    title: String
    url: String
    isUploaded: Boolean
}